// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { type2Object } from './util/CreateObjectHelper';

// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "tstype2obejct" is now active!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with registerCommand
	// The commandId parameter must match the command field in package.json
	let disposable = vscode.commands.registerCommand('tstype2obejct.tstype2obejct', async () => {
		// The code you place here will be executed every time your command is executed
		// Display a message box to the user
        // 提示信息
		vscode.window.showInformationMessage('start to tstype2obejct');
        // 获取编辑器编辑区
        const editor = vscode.window.activeTextEditor;
        if (editor) {
            // 获取选择的区域
            const { document, selection } = editor;
            const text = document.getText(selection);
            const newText2 = await type2Object(text);
            console.log('已选择的文字', newText2);
            editor.edit((editBuilder) => {
                // 在选择区域下方插入文字
                editBuilder.insert(selection.end, `\n${newText2}`);
            });
            vscode.window.showInformationMessage('end to tstype2obejct');
        }
	});
	context.subscriptions.push(disposable);
}

// This method is called when your extension is deactivated
export function deactivate() {}
